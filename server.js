const express = require('express');
const services = require('./api/services');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));


app.use('/api/v1/services', services);


app.use((req, res, next) => {
  const err = new Error('Not found');
  err.status = 404;
  next(err);
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  res.json({
    message: err.message,
    error: process.env.NODE_ENV === 'production' ? {} : err,
  });
});

const PORT = process.env.PORT || 8000;
app.listen(PORT, () => {
  console.log(`listening on PORT ${PORT}`);
});
