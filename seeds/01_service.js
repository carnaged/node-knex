
exports.seed = (knex) => knex('service').del()
  .then(() => knex('service').insert([
    {
      name: 'hair',
      description: 'Cut hair',
      price: 100,
      time_req: '18088880',
    },
    {
      name: 'shave',
      description: 'Clean shave face',
      price: 80,
      time_req: '18084880',
    },
    {
      name: 'beard',
      description: 'Shape beard',
      price: 120,
      time_req: '17088880',
    },
  ]));
