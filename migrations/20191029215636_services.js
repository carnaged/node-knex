
exports.up = (knex) => knex.schema.createTable('service', (table) => {
  table.increments();
  table.text('name');
  table.text('description');
  table.float('price');
  table.date('time_req');
});

exports.down = (knex) => {
  knex.schema.dropTable('service');
};
