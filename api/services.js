const router = require('express').Router();
const queries = require('../db/queries');

router.get('/', (req, res) => {
  queries.getAll().then((services) => {
    res.json(services);
  });
});

module.exports = router;
